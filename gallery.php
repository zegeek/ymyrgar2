<?php $link = get_bloginfo('template_directory').'/'; ?>
				</div>
			</header>
<!--==============================content================================-->
			<section id="content">
				<div class="main">
						<div class="container gallery-content">
							<div class="row">
								<div class="col s12">
									<h3 class="letter">Our <strong>Gallery</strong></h3>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
								<?php
								$query_images_args = array(
									'post_type' => 'attachment', 'post_mime_type' =>'image', 'post_status' => 'inherit', 'posts_per_page' => -1,
								);
								$query_images = new WP_Query( $query_images_args );
								$id_image = array();
								$i = 0;
								foreach ( $query_images->posts as $media) {
									$mediaID = $media->ID;
									$id_image[$i] = $mediaID;
									$i++;
								}
							?>
									<div class="slideshow-container">
									<?php 
									foreach($id_image as $id) 
									{
										echo '<div class="slideshow-container-element">'.wp_get_attachment_image($id,array('962','474'),"").'</div>';
										//echo '<div class="slideshow-container-element"><img src="'.wp_get_attachment_url( $id).'" height="474"></div>';
									}
									?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="slideshow">
									<?php 
									foreach($id_image as $id) 
									{
										echo '<div class="slideshow-element">'.wp_get_attachment_image($id,'thumbnail').'</div>';
									}
									?>
									</div>
								</div>
							</div>
						</div>			
				</div>
				<div class="block"></div>
			</section>
			<script type="text/javascript">
			$(document).ready(function() {
				$('div.navigation').css({'width' : '920px', 'float' : 'left'});
				$('div.content').css('display', 'block');
				$('.slideshow-container').slick({
  					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					fade: true,
					asNavFor: '.slideshow'
				});
				$('.slideshow').slick({
					arrows: false,
					slidesToShow: 3,
					slidesToScroll: 1,
					asNavFor: '.slideshow-container',
					dots: false,
					centerMode: true,
					focusOnSelect: true
				});
			});
		</script>
		</div>
<!--==============================footer=================================-->
