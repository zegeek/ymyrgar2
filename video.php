				</div>
			</header>
<!--==============================content================================-->
			<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com. November 21, 2011!</div>
				<div class="main">
					<div class="content-padding-2">
						<div class="container_12">
							<div class="wrapper">
								<div class="grid_12">
									<div class="padding-grid-1">
										<h3 class="letter">Our <strong>Music Videos</strong></h3>
									</div>
									<?php
									$args = array(
									'post_type' => 'video_links',
									'posts_per_page' => -1,
									'order' => 'DESC',
									'orderby' => 'menu_order'
								);

								$the_query = new WP_Query($args);
								if ($the_query->have_posts()) :
									$counter = 0;
									while ($the_query->have_posts()) : $the_query->the_post();
										if ($counter % 3 == 0) :
											echo $counter > 0 ? "</div>" : ""; // close div if it's not the first
											echo '<div class="wrapper indent-bot1">';
										endif;
										$link = get_field('link');
										?>
										<article class="grid_4 alpha">
											<div class="padding-grid-3">
												<div class="wrapper p2">
													<iframe width="252" height="131" src="<?php echo $link; ?>"  frameborder="0" allowfullscreen></iframe>
												</div>
											</div>
										</article>
										<?php
										$counter++;

									endwhile;
								endif;
								wp_reset_postdata();
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<div class="block"></div>
			</section>
		</div>
		<script type="text/javascript">
			$(window).load(function(){
				$("a[data-gal^='prettyVideo']").prettyPhoto({animation_speed:'normal',theme:'facebook',slideshow:false, autoplay_slideshow: false});
				$("a[data-gal^='prettyVideo_1']").prettyPhoto({animation_speed:'normal',theme:'facebook',slideshow:false, autoplay_slideshow: false});
				$("a[data-gal^='prettyVideo_2']").prettyPhoto({animation_speed:'normal',theme:'facebook',slideshow:false, autoplay_slideshow: false});
				$("a[data-gal^='prettyVideo_3']").prettyPhoto({animation_speed:'normal',theme:'facebook',slideshow:false, autoplay_slideshow: false});
				$("a[data-gal^='prettyVideo_4']").prettyPhoto({animation_speed:'normal',theme:'facebook',slideshow:false, autoplay_slideshow: false});
				$("a[data-gal^='prettyVideo_5']").prettyPhoto({animation_speed:'normal',theme:'facebook',slideshow:false, autoplay_slideshow: false});
			}); 
		</script>
		<script type="text/javascript"> Cufon.now(); </script>
<!--==============================footer=================================-->