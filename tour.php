</div>
</header>
<section id="content">
	<div class="ic">More Website Templates @ TemplateMonster.com. November 21, 2011!</div>
	<div class="main">
		<div class="content-padding-2">
			<div class="container_12">
				<div class="row">
					<div class="col s12 m8">
						<div class="padding-grid-1">
							<h3>Tour <strong>Dates</strong></h3>
						</div>
						<div class="row">
							<article class="col s12 alpha">
								<div class="padding-grid-1">
									<?php 
									
									$today = date('Ymd');
                        $args = array( 'post_type' => 'tour', 
                                'meta_key'  => 'date', 
                                'orderby'  => 'meta_value', 
                                'order'    => 'ASC',
								'meta_query' => array(
											array(
												'key'		=> 'date',
												'compare'	=> '>=',
												'value'		=> $today,
											)
										)
								
									 ); 
                        $the_query = new WP_Query( $args );  
                        ?>
									<?php if ( $the_query->have_posts() ) : ?>
									<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
									<?php 
                          get_template_part( 'date-tour-page', get_post_format() ); 
                        ?>
									<?php endwhile; endif; ?>
									<?php wp_reset_query(); ?>
								</div>
							</article>
						</div>
					</div>
					<article class="col m4 s12 alpha">
						<div class="padding-grid-3">
							<h3>Past <strong>Dates</strong></h3>
							<ul class="list-3">
								<!-- list past tours-->
								
									<?php 
                    
                        				$today = date('Ymd');
                        $args = array( 'post_type' => 'tour', 
                                'meta_key'  => 'date', 
                                'orderby'  => 'meta_value', 
                                'order'    => 'ASC',
								'meta_query' => array(
											array(
												'key'		=> 'date',
												'compare'	=> '<',
												'value'		=> $today,
											)
										)
								
									 ); 
                        $the_query = new WP_Query( $args );  
                        ?>
									<?php if ( $the_query->have_posts() ) : ?>
									<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
									<?php 
                          get_template_part( 'past-date-tour-page', get_post_format() ); 
                        ?>
									<?php endwhile; endif; ?>
									<?php wp_reset_query(); ?>
								

							</ul>
						</div>
					</article>
				</div>
			</div>
			<div class="block"></div>
		</div>
	</div>
</section>
