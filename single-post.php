<?php get_header(); ?>
    </div>
    </header>
    <section id="content">
				<div class="main">
					<div class="bg-2">
						<div class="content-padding-1">
							<div class="container_12">
								<div class="wrapper">
									<article class="grid_12">
                                        <div class="padding-grid-1">
                                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                        <?php $my_date = the_date('Y-m-d.d.M','','',false); 
                                            $date_element = explode(".",$my_date);?>
                                        <div class="wrapper img-indent-bot1">
                                            <time class="time time-stule-1" datetime="<?php echo $date_element[0]; ?>"> <strong class="text-1"><?php echo $date_element[1]; ?></strong><strong class="text-2"><?php echo $date_element[2]; ?></strong></time>
                                            <div class="extra-wrap">
                                                <h4 class="indent-top"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                <div class="indent-top">
                                                    <div><?php the_content(); ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endwhile; endif; ?>
                                        </div>
                                    </article>
                                </div>
                                <div class="right-align row">
                                    <div class="col s12 m2 offset-m10">
                                        <a href="<?php echo get_bloginfo( 'wpurl' );?>">Back to home</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block"></div>
    </section>
    <?php
get_footer();
?>