<?php $link = get_bloginfo('template_directory').'/'; ?>
<div class="slider-wrapper">
	<div class="slider-cont">
		<?php
								$query_images_args = array(
									'post_type' => 'attachment', 'post_mime_type' =>'image', 'post_status' => 'inherit', 'posts_per_page' => -1,
								);
								$query_images = new WP_Query( $query_images_args );
								$id_image = array();
								$i = 0;
								foreach ( $query_images->posts as $media) {
									if($media->post_content == "slider")
									{
										$mediaID = $media->ID;
										$id_image[$i] = $mediaID;
										$i++;
									}
								}
								$rand = array_rand($id_image,3);
								for($i = 0; $i < 3; $i++)
								{
									echo '<div class="slider-item" >'.wp_get_attachment_image($id_image[$rand[$i]],'slider-main').'</div>';
								}
							?>
	</div>
</div>
</div>
</header>
<section id="content">
	<div class="main">
		<div class="bg-2">
			<div class="content-padding-1">
				<div class="container_12">
					<div class="wrapper">
						<article class="grid_4">
							<div class="padding-grid-1">
								<h3 class="letter">Latest <strong>News</strong></h3>
								<?php 
											$args = array( 'post_type' => 'post', 'posts_per_page' => 3 );
											$the_query = new WP_Query( $args ); 
			                                if ( $the_query->have_posts() ) : while ( $the_query->have_posts()  ) : $the_query->the_post();;
  	
				                                get_template_part( 'post', get_post_format() );
  
			                                endwhile; endif; 
                                            ?>
								<?php wp_reset_query(); ?>
							</div>
						</article>
						<article class="grid_4 alpha">
							<div class="padding-grid-1">
								<h3>Tour <strong>Dates</strong></h3>
								<?php
										
								$today = date('Ymd');
                        $args = array( 'post_type' => 'tour', 
                                'meta_key'  => 'date', 
                                'orderby'  => 'meta_value', 
                                'order'    => 'ASC',
								'posts_per_page' => 3,
								'meta_query' => array(
											array(
												'key'		=> 'date',
												'compare'	=> '>=',
												'value'		=> $today,
											)
										)
								
									 ); 
												 
                        $the_query = new WP_Query( $args ); 
												$the_query = new WP_Query( $args ); 
												?>
									<?php if ( $the_query->have_posts() ) : ?>
									<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
									<?php
													get_template_part( 'date-home', get_post_format() );
												?>
										<?php endwhile; endif; ?>
										<?php wp_reset_query(); ?>
							</div>
						</article>
						<article class="grid_4 alpha">
							<div class="padding-grid-2">
								<h3 class="letter">Our <strong>Shop</strong></h3>
								<div class="wrapper">
									<figure class="style-img fleft"><a href="http://ymyrgar.bigcartel.com" action="_blank"><img src="<?php echo $link; ?>images/page1-img1.jpg"  alt=""></a></figure>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
		<div class="content-padding-2">
			<div class="container_12">
				<div class="wrapper">
				<!--
					<article class="grid_4">
						<div class="padding-grid-1">
							<h3>Upcoming <strong>Events</strong></h3>
							<div class="wrapper img-indent-bot1">
							
								<time class="time time-stule-3" datetime="2011-11-09"> <strong class="text-5">09</strong><strong class="text-6">nov</strong></time>
								<div class="extra-wrap">
									<div class="indent-top">
										Lorem ipsum dolor consctetur adipisicing elitdo eusmod tempor incididunt ut labore.
									</div>
								</div>
								
							</div>
							<div class="wrapper">
							
								<time class="time time-stule-3" datetime="2011-11-03"> <strong class="text-5">03</strong><strong class="text-6">nov</strong></time>
								<div class="extra-wrap">
									<div class="indent-top">
										Lorem ipsum dolor consctetur adipisicing elitdo eusmod tempor incididunt ut labore.
									</div>
								</div>
							
							</div>
						</div>
					</article>
					-->
					<article class="grid_6 alpha">
						<div class="padding-grid-2">
							<h3 class="letter">Latest <strong>Video</strong></h3>
							<?php 
							$args = array( 'post_type' => 'video_links', 
								'posts_per_page' => 1,
								'meta_query' => array(
											array(
												'key'		=> 'display_on',
												'compare'	=> '==',
												'value'		=> '1',
											)
										)
								
									 ); 
									 $the_query = new WP_Query( $args ); 
									?>
									<?php if ( $the_query->have_posts() ) : ?>
									<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
									<?php $link = get_field('link'); ?>
							<div class="wrapper">
								<figure class="style-img-2 fleft"><iframe width="252" height="131" src="<?php echo $link; ?>"  frameborder="0" allowfullscreen></iframe></figure>
							</div>
							<?php endwhile; endif; ?>
										<?php wp_reset_query(); ?>
						</div>
					</article>
					<article class="grid_6 alpha">
						<div class="padding-grid-2">
							<h3 class="letter prev-indent-bot1">Shortly <strong>About</strong></h3>
							<h6>Ymyrgar...</h6>
							<?php echo get_bloginfo( 'description' ); ?>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
	<div class="block"></div>
</section>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.slider-cont').slick({
			autoplay: true,
			speed: 1000,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			adaptiveHeight: true,
			focusOnSelect: false
		});
		$("a[data-gal^='prettyVideo']").prettyPhoto({
			animation_speed: 'normal',
			theme: 'facebook',
			slideshow: false,
			autoplay_slideshow: false
		});
	});
	Cufon.now();

</script>
