<?php 
    $months = array("01" => "Jan", 
                "02" => "Feb", 
                "03" => "Mar", 
                "04" => "Apr", 
                "05" => "Mai", 
                "06" => "Jun", 
                "07" => "Jul", 
                "08" => "Aug", 
                "09" => "Sep", 
                "10" => "Oct", 
                "11" => "Nov", 
                "12" => "Dec"); 
    $date = get_field('date'); 
    $date_elem = explode("/", $date); 
    $place = get_field('place'); 
    $country = get_field('country'); 
    $link = get_field('link'); 
  $tel=get_field('contact'); 
?>
<li>
	<div class="row img-indent-bot2">
		<div class="col s12 m12">
			<div class="extra-wrap">
				<h4 class="indent-top">
					<?php the_title(); echo ", ".$place; ?>
				</h4>

			</div>
			<span> <?php echo $date_elem[0]; ?> <?php echo $months[$date_elem[1]]; ?></span> <span><?php echo $date_elem[2]; ?></span>

		</div>
	</div>
</li>
