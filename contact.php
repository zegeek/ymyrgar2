</div>
</header>
<!--==============================content================================-->
<section id="content">
	<div class="ic">More Website Templates @ TemplateMonster.com. November 21, 2011!</div>
	<div class="main">
		<div class="content-padding-2">
			<div class="container no-margin full-width">
				<div class="row full-width">
					<article class="col m8 s12">
						<div class="padding-grid-1">
							<h3>Contact <strong>Form</strong></h3>
							<?php echo do_shortcode( '[contact-form-7 id="37" title="Contact_form"]' ); ?>

						</div>
					</article>
					<article class="col m4 s12 alpha">
						<div class="padding-grid-3">
							<h3>Contact <strong>Info</strong></h3>
							<div class="wrapper p2">
								<figure class="style-img-2 fleft">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1596.3562702193656!2d10.181657597195898!3d36.84935999735043!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDUwJzU3LjciTiAxMMKwMTAnNTYuOCJF!5e0!3m2!1sen!2sfr!4v1498059725630" width="252" height="195" frameborder="0" style="border:0" allowfullscreen></iframe>
								</figure>
							</div>
							<dl class="list-4">
								<!--<dt>8901 Marmora Road,<br>Glasgow, D04 89GR.</dt>-->
								<dd>TUN: +216 52 866 381 <br> GER: +49 01 76 37 78 20 06</dd>
								
								<dd>E-mail: <a class="link"> contact@ymyrgar.net</a></dd>
							</dl>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
	<div class="block"></div>
</section>
</div>
