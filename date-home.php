<?php /*$custom_fields = get_post_custom(); 
      $terms = wp_get_post_terms( get_the_ID(), array( 'place', 'date','country' ) );
      $elem = array('place','date','country');
      foreach ( $terms as $term ) :
        if($term->taxonomy == 'place') {
            $elem['place'] = $term->name;
        } 
        else if($term->taxonomy == 'date')
        {
            $elem['date'] = $term->name;
        }
        else 
        {
            $elem['country'] = $term->name;
        }
    endforeach; */
    $months = array("01" => "Jan",
                "02" => "Feb",
                "03" => "Mar",
                "04" => "Apr",
                "05" => "Mai",
                "06" => "Jun",
                "07" => "Jul",
                "08" => "Aug",
                "09" => "Sep",
                "10" => "Oct",
                "11" => "Nov",
                "12" => "Dec");
    $date = get_field('date');
    $date_elem = explode("/", $date);
    $place = get_field('place');
    $country = get_field('country');
    $link = get_field('link');
    $tour = get_field('tour');
?>
<div class="wrapper img-indent-bot2">
	<time class="time time-stule-2" datetime="<?php echo $date; ?>"> <strong class="text-3"><?php echo $date_elem[0]; ?></strong><strong class="text-4"><?php echo $months[$date_elem[1]]; ?></strong></time>
	<div class="extra-wrap">
	    <h4 class="indent-top"><a href="<?php echo $link; ?>" target="_blank"><?php the_title(); echo ', '.$place; ?></a></h4>
		<?php echo $country; ?>
		</div>
	</div>