<?php
add_image_size('slider-main',962,368);
class my_walker extends Walker_page {
    function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $output .= '<div class="col m2 s12 item"><a href="'.get_permalink($object->ID).'">'.$object->post_title;
    }

    function end_el( &$output, $object, $depth = 0, $args = array() ) {
        $output .= "</a></div>\n";
    }
}
?>