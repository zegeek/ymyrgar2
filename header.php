<?php $link = get_bloginfo('template_directory').'/'; 
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo get_bloginfo( 'name' ); ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link type="text/css" rel="stylesheet" href="<?php echo $link; ?>css/materialize.min.css"  media="screen,projection"/>
		<link rel="stylesheet" href="<?php echo $link; ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo $link; ?>css/grid.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo $link; ?>css/slick.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo $link; ?>css/prettyPhoto.css" type="text/css" media="screen">   
		<script src="<?php echo $link; ?>js/jquery-3.2.1.min.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/jquery-migrate-3.0.0.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $link; ?>js/materialize.min.js"></script>
		<script type="text/javascript" src="<?php echo $link; ?>js/slick.js"></script>
		<script src="<?php echo $link; ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/Vegur_700.font.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/Vegur_400.font.js" type="text/javascript"></script> 
		<!--<script src="js/FF-cash.js" type="text/javascript"></script> -->
		<script src="<?php echo $link; ?>js/script.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $link; ?>js/easyTooltip.js"></script>
		<script src="<?php echo $link; ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/hover-image.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
		<script src="<?php echo $link; ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $link; ?>js/tms-0.3.js"></script>
		<script type="text/javascript" src="<?php echo $link; ?>js/tms_presets.js"></script>		
		<!--[if lt IE 7]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
		<![endif]-->
		<!--[if lt IE 9]>
			<script type="text/javascript" src="js/html5.js"></script>
			<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
		<?php wp_head();?>
	</head>
	<body id="page1">
		<div class="extra">
<!--==============================header=================================-->
			<header>
				<div class="main container">
					<div class="bg-1 row">
						<div class="col s12 bg-min-height valign-wrapper">
							<div class="main-logo"></div>
						</div>
					</div>
					<nav>
						<div class="menu-bg-tail">
							<div class="menu-bg">
								<div class="container menu-cont">
									<div class="row menu-cont my-row valign-wrapper">
										<div class="col m2 s12 menu-item" show-menu="no"><a>Menu</a></div>
										<?php
											wp_list_pages(array(
												'sort_column' => 'menu_order',
												'title_li'    => '',
												'echo'        => 1,
												'post_type ' => 'page',
												'walker' => new my_walker()
											)); 
										?>
										<!--<div class="col m2 s12 item"><a class="active" href="<?php echo get_bloginfo( 'wpurl' );?>">About</a></div>
										<div class="col m2 s12 item"><a href="audio.php">Audio</a></div>
										<div class="col m2 s12 item"><a href="video.php">Video</a></div>
										<div class="col m2 s12 item"><a href="gallery.php">Gallery</a></div>
										<div class="col m2 s12 item"><a href="tour-dates.php">Tour</a></div>
										<div class="col m2 s12 item"><a href="contacts.php">Contacts</a></div> -->
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</nav>