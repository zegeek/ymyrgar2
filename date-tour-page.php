<?php 
    $months = array("01" => "Jan", 
                "02" => "Feb", 
                "03" => "Mar", 
                "04" => "Apr", 
                "05" => "Mai", 
                "06" => "Jun", 
                "07" => "Jul", 
                "08" => "Aug", 
                "09" => "Sep", 
                "10" => "Oct", 
                "11" => "Nov", 
                "12" => "Dec"); 
    $date = get_field('date'); 
    $date_elem = explode("/", $date); 
    $place = get_field('place'); 
    $country = get_field('country'); 
    $link = get_field('link'); 
  $tel=get_field('contact'); 
?> 
<div class="row img-indent-bot2"> 
  <div class="col s12 m6"> 
    <time class="time time-stule-2" datetime="<?php echo $date; ?>"> <strong class="text-3"><?php echo $date_elem[0]; ?></strong><strong class="text-4"><?php echo $months[$date_elem[1]]; ?></strong></time> 
    <div class="extra-wrap"> 
      <h4 class="indent-top"><?php the_title(); echo ", ".$place; ?></h4> 
		<span><?php echo $date_elem[2]; ?></span>
    </div> 
  </div> 
  <div class="col s12 m6"> 
    <div class="margin-bot1"> 
      <span class="text-width">Tickets:</span> <span class="inline"><a class="link" href="<?php echo $link; ?>">Link</a></span> 
      <span class="text-width">Telephone:</span> <span class="color-1 inline"><?php echo $tel; ?></span> 
    </div> 
  </div> 
</div>