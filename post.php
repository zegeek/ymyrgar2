<?php $my_date = the_date('Y-m-d.d.M','','',false);
      $date_element = explode(".",$my_date);?>
<div class="wrapper img-indent-bot1">
	<time class="time time-stule-1" datetime="<?php echo $date_element[0]; ?>"> <strong class="text-1"><?php echo $date_element[1]; ?></strong><strong class="text-2"><?php echo $date_element[2]; ?></strong></time>
	<div class="extra-wrap">
		<h4 class="indent-top"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<div class="indent-top">
			<div class="truncate"><?php the_content(); ?></div>
		</div>
	</div>
</div>